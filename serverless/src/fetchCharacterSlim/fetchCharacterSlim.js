const fetchCharacter = require('../fetchCharacterFactory');

module.exports = event => fetchCharacter(true, event);