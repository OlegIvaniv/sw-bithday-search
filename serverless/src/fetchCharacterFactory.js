/* 
  Slim version fetches only data that don't need another request 
  which makes first load faster. 
*/

const fetch = require('isomorphic-fetch');
const fetchAndProcess = async (fetchUrl) => {
  const response = await fetch(fetchUrl);
  return response.json();
};
const SWAPI_ENDPOINT = 'https://swapi.co/api/people/';

module.exports = async (isSlim, event) => {
  const { name } = event ? event.data : false;
  const charactersFetchUrl = name ? SWAPI_ENDPOINT + '?search=' + name : SWAPI_ENDPOINT;
  const { results } = await fetchAndProcess(charactersFetchUrl);

  // Itterate over results and transform data
  // Map methods is going to return list of promises(vows? :-D) so we 
  // have to await and resolve them all
  const transformedCharacters = await Promise.all(results.map(async (character) => {
    let characterData = {
      name: character.name,
      homeworld: character.homeworld,
      birthYear: character.birth_year,
      infoLink: character.url,
    }
    if (!isSlim) {
      // Get list of films URLs
      const { films } = await character;
      const filmsList = await Promise.all(films.map(async (film) => {
        // Fetch each film URL and return title
        const { title } = await fetchAndProcess(film);
        return title;
      }))
      characterData.appearsIn = filmsList;
      return characterData
    } else {
      return characterData;
    }
  }));

  return {
    data: transformedCharacters
  };
}