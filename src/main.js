import 'promise-polyfill/src/polyfill';

import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { fetch } from 'isomorphic-fetch';
import { InMemoryCache } from 'apollo-cache-inmemory';
import Vue from 'vue';
import VueApollo from 'vue-apollo';

import App from './App.vue';

const httpLink = new HttpLink({
  uri: 'https://api.graph.cool/simple/v1/cjfcotzjm1e8h01551p6ux29e',
  fetch,
});

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
  connectToDevTools: true,
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

// Install the vue plugin
Vue.use(VueApollo);

Vue.config.productionTip = false;

new Vue({
  provide: apolloProvider.provide(),
  render: h => h(App),
}).$mount('#app');
